import 'package:flutter/material.dart';
import 'package:forum_flutterando/src/info/info-widget.dart';
import 'package:forum_flutterando/src/webview/webview-widget.dart';

final routes = {
  '/': (BuildContext context) => WebViewPage(),
  '/info': (BuildContext context) => InfoPage(),
  };
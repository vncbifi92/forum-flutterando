import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class InfoPage extends StatefulWidget {
  InfoPage({Key key}) : super(key: key);

  @override
  _InfoPageState createState() => new _InfoPageState();
}

class _InfoPageState extends State<InfoPage>{

  @override
  Widget build(BuildContext context) {

    Widget _sobre(){
      return Container(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Center(child: Image(image: AssetImage('assets/images/1.jpg'))),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Html(
                    defaultTextStyle: TextStyle(color: Colors.black54),
                    data: "<h2>Bem-vindo ao Flutterando, a comunidade referência em Flutter no Brasil!</h2>"
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Html(
                      defaultTextStyle: TextStyle(color: Colors.black54),
                      data: "</br></br><h3>Terças: Lives no canal do YouTube;</br>Quintas: Gravações de podcasts no Discord;</br>Sábado: Desafios em equipe;</h3>"
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Html(
                      defaultTextStyle: TextStyle(color: Colors.black54),
                      onLinkTap: (url)async{
                        launch(url);
                      },
                      data: "</br></br></h4>Para dicas, tutoriais e discussões sérias sobre Flutter, acesse o link do grupo: <a href='http://t.me/flutterando'>Flutterando</a>.</h4></br></h4>Fique por dentro e participe dos desafios semanais, acesse o canal: <a href='http://t.me/flutterando_desafios'>Flutterando Desafios</a>.</h4></br></h4>Para conversas off-topic, acesse o grupo: <a href='http://t.me/flutterandodadepressao'>Flutterando da Depressão</a>.</h4></br></h4>Para oportunidades de trabalhos com Flutter, acesse o canal: <a href='http://t.me/flutterandoclassificados'>Flutterando Classificados</a>.</h4></br></h4>Para links relacionados a Flutter/Dart e afins, acesse o canal: <a href='http://t.me/flutterandolinks'>Flutterando Links</a>.</h4></br></h4>Acesse o blog oficial do grupo em: <a href='http://flutterando.com.br'>flutterando.com.br</a></h4>"
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }


    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
              tooltip: "Forum",
              icon: Icon(
                Icons.home,
                color: Colors.blue,
              ),
              onPressed: (){Navigator.of(context).pushReplacementNamed('/');}
          )
        ],
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          "Sobre",
          style: TextStyle(
              color: Colors.blue
          ),
        ),
      ),
      body: _sobre(),
    );

  }
}
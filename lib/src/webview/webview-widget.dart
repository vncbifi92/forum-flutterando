import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebViewPage extends StatefulWidget {
  WebViewPage({Key key}) : super(key: key);

  @override
  _WebViewPageState createState() => new _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage>{

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: "https://forum.flutterando.com.br",
      withJavascript: true,
      withLocalStorage: true,
      withLocalUrl: true,
      allowFileURLs: true,
      hidden: true,
      enableAppScheme: true,
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
              tooltip: "Info",
              icon: Icon(
                Icons.info,
                color: Colors.blue,
              ),
              onPressed: (){Navigator.of(context).pushReplacementNamed('/info');}
          )
        ],
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          "Forum Flutterando",
          style: TextStyle(
              color: Colors.blue
          ),
        )
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:forum_flutterando/src/shared/routes.dart';
import 'package:forum_flutterando/src/webview/webview-widget.dart';


class AppWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      routes: routes,
      debugShowCheckedModeBanner: false,
    );
  }
}